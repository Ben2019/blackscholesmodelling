import pandas as pd
import numpy as np
import scipy.stats as si
import sympy as sy

from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import os

# Question 1
def analytical_bs_formula(S, K, T, r, q, sigma):
    # S: stock price
    # K: strike price
    # T: time to expirartion
    # r: risk-free interest rate
    # q: dividend yield
    # sigma: exercise price


    d1 = (np.log(S / K) + (r -q + 0.5 * sigma ** 2) * T) / (sigma * np.sqrt(T))
    d2 = (np.log(S / K) + (r - q - 0.5 * sigma ** 2) * T) / (sigma * np.sqrt(T))

    call = (S * si.norm.cdf(d1, 0.0, 1.0) - K * np.exp(-r * T) * si.norm.cdf(d2, 0.0, 1.0))
    return call

print(analytical_bs_formula(120, 100, 1, 0.01, 0.02, 1))

